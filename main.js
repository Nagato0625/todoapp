onst form = document.querySelector(".input-form");
const input = document.querySelector(".input");
const modalInput = document.querySelector(".modal-input");
const addBtn = document.querySelector(".add-btn");
const modalBtn = document.querySelector(".modal-btn");
const todoList = document.querySelector(".todo-list");
const todoFilterItems = document.querySelectorAll(".todo-filter-item");
const todoFIlterItemsArray = Array.from(todoFilterItems);
const modal = document.querySelector(".modal");
const modalClose = document.querySelector(".modal-close");
const dateArea = document.querySelector(".date");
const subMenuArea = document.querySelector(".todo-nav-filter");
const priorityRadios = document.querySelectorAll(".priority-check-item");
const modalPriorityRadios = document.querySelectorAll(".modal-priority");
const clearAllBtn = document.querySelector(".clear-btn");
const user = document.querySelector(".user");
const userName = document.querySelector(".user-name");



document.addEventListener("DOMContentLoaded", () => {
    const today = new Date();
    const months = ["January", "Feburary", "March", "April", "May", "Jun", "July", "August", "September", "October", "Novenmber", "December"];
    const month = months[today.getMonth()];
    const weekDay = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Staturday"];
    const day = weekDay[today.getDay()];
    const date  = today.getDate();
    dateArea.innerHTML = `${day}, ${month} ${date}`;
    
    displayTodo();
});



/* set */
const getCheckedRadio = () => {
    const radioArray = Array.from(priorityRadios);
    const res = radioArray.find(radio => {
        return radio.firstElementChild.checked;
    });
    return res.firstElementChild;
};

const getModalCheckRadio = () => {
    const modalRadioArray = Array.from(modalPriorityRadios);
    const res = modalRadioArray.find(radio => {
        return radio.firstElementChild.checked;
    });
    return res.firstElementChild;
};

const setTodo = (eId) => {
    const inputValue = input.value;
    const radioArray = Array.from(priorityRadios);
    const radio = radioArray.map(radio => {
        return radio.firstElementChild;
    });
    let todos = getTodos();
    if(eId) {
        const editableTodo = todos.find(todo => {
            return todo.id == eId;
        })
        editableTodo.ttl = modalInput.value;
        const radio = getModalCheckRadio();
        editableTodo.priority = radio.value;
    } else {
        if(inputValue !== "") {
            const radio = getCheckedRadio();
            const random = Math.floor(Math.random() * 100);
            todos.push({"ttl": inputValue, "isCompleted": false, "id": random, "isFaved": false, priority: radio.value});
        }
    }
        localStorage.setItem("todos", JSON.stringify(todos));
        input.value = "";
        radio.checked = false;
    displayTodo();
};

form.addEventListener("submit", (e) => {
    e.preventDefault();
    setTodo();
});

addBtn.addEventListener("click", () => {
    setTodo();
});


/* display */
const displayTodo = (id) => {
    let todos = getTodos();
    let html = "";
    if(id) {
        todos = todos.filter(todo => {
            return todo.id == id;
        });
    };
    todos.map(todo => {
    html += `
    <li class="todo-item" data-priority=${todo.priority}>
      <div class="todo-item-info ${todo.isCompleted && "completed"}">
        <input class="todo-check" type="checkbox" ${todo.isCompleted && "checked"}>
        <p class="todo-ttl ${todo.priority}">${todo.ttl}</p>
      </div>
      <div class="todo-item-icons" id=${todo.id}>
        <i class="fas fa-edit edit"></i>
        <i class="fas fa-trash trash"></i>
        <i class="far fa-star star ${todo.isFaved && "stared"}"></i>
      </div>
    </li>
    `
  });
  todoList.innerHTML = html;
};



/* edit */
let eId = "";
todoList.addEventListener("click", (e) => {
    const target = e.target;
    if(target.classList.contains("edit")) {
        modal.classList.add("open");
        const aa = target.parentElement.previousElementSibling.lastElementChild.innerText;
        const todos  =Array.from(todoList.children);
        const editableTodo = todos.find(todo => {
            return todo.lastElementChild.id == target.parentElement.id;
        });
        eId = editableTodo.lastElementChild.id;
        modalInput.value = editableTodo.firstElementChild.lastElementChild.innerText;
    };
});

modalClose.addEventListener("click", () => {
    modal.classList.remove("open");
})

modalBtn.addEventListener("click", (e) => {
    setTodo(eId);
    modal.classList.remove("open");
});
/* edit */



/* delete */
const deleteTodo = (id) => {
    let todos = getTodos();
    const newTodos = todos.filter(todo => {
      return todo.id !== id;
    })
    localStorage.setItem("todos", JSON.stringify(newTodos));
    displayTodo();
  };
todoList.addEventListener("click", (e) => {
  if(e.target.classList.contains("trash")) {
    const id = Number(e.target.parentElement.id);
    deleteTodo(id);
  }
});
/* delete */



/* check */
todoList.addEventListener("click", (e) => {
    let todos = getTodos();
    if(e.target.classList.contains("todo-check")) {
        const id = Number(e.target.parentElement.nextElementSibling.id);
        const checkedTodo = todos.find(todo => {
            return todo.id == id;
        });
        if(!checkedTodo.isCompleted) {
            checkedTodo.isCompleted = true;
        } else {
            checkedTodo.isCompleted = false;
        }
        localStorage.setItem("todos", JSON.stringify(todos));
        displayTodo();
    };
});
/* check */



/* star */
todoList.addEventListener("click", (e) => {
    let todos = getTodos();
    if(e.target.classList.contains("star")) {
        const id = Number(e.target.parentElement.id);
        const staredTodo = todos.find(todo => {
            return todo.id == id;
        });
        if(!staredTodo.isFaved) {
            staredTodo.isFaved = true;
        } else {
            staredTodo.isFaved = false;
        }
        localStorage.setItem("todos", JSON.stringify(todos));
        displayTodo();
    };
});
/* star */



/* filter */
const filterTodo = (e) => {
    let todos = todoList.children;
    todos = Array.from(todos);
    
    todos.forEach(todo => {
        switch(e.target.dataset.nav){
            case "all":
                todo.style.display = "flex";
                break;
            case "completed":
                if(todo.firstElementChild.classList.contains("completed")) {
                    todo.style.display = "flex";
                } else {
                    todo.style.display = "none";
                };
                break;
            case "unCompleted":
                if(!todo.firstElementChild.classList.contains("completed")) {
                    todo.style.display = "flex";
                } else {
                    todo.style.display = "none";
                };
                break;
            case "stared":
                if(todo.lastElementChild.lastElementChild.classList.contains("stared")) {
                    todo.style.display = "flex";
                } else {
                    todo.style.display = "none";
                };
                break
            case "high":
                if(todo.firstElementChild.lastElementChild.classList.contains("high")) {
                    todo.style.display = "flex";
                } else {
                    todo.style.display = "none";
                };
                break
            case "medium":
                if(todo.firstElementChild.lastElementChild.classList.contains("medium")) {
                    todo.style.display = "flex";
                } else {
                    todo.style.display = "none";
                };
                break
            case "low":
                if(todo.firstElementChild.lastElementChild.classList.contains("low")) {
                    todo.style.display = "flex";
                } else {
                    todo.style.display = "none";
                };
                break        
        }
    })
};

if(todoFIlterItemsArray) {
    todoFIlterItemsArray.forEach(item => {
        item.addEventListener("click", filterTodo);
    });
};
/* filter */



/* get todos */
const getTodos = () => {
    let todos;
    if(localStorage.getItem("todos") === null) {
        return todos = [];
    } else {
        return todos = JSON.parse(localStorage.getItem("todos"));
    };
};
/* get todos */


/* clear all todos */
clearAllBtn.addEventListener("click", () => {
    const res = window.confirm("Clear all todos?");
    if(res) {
        localStorage.clear();
        displayTodo()
    }
});
/* clear all todos */


/* user info */
user.addEventListener("click", () => {
    let todos = getTodos()
    let messeage = ""
    if(todos.length == 0) {
        messeage = `Hello ${userName.innerText}, you have nothing in your list`;
        window.alert(messeage);
    } else if(todos.length == 1) {
        messeage = `Hello ${userName.innerText}, you have a todo today`;
        window.alert(messeage);
    } else {
        messeage = `Hello ${userName.innerText}, you have ${todos.length} todos today`;
        window.alert(messeage);
    }
}) 
/* user info */