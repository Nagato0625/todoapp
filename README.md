## Name 
Todo app for a coding test


## Description 
a todo app created with vanila JS, HTML and CSS.
You can add, delete and  edit todos. 
You can prioritize todos.
You can highlight todos(favorite or important todo or set todo as what you wanna see later)
You can mark todos to see if you've done the todo.
You can see categorized todos with all, completed, uncompleted, stared, high(priority), medium(priority) and low(priority).  